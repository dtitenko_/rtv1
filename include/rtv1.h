/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:34:41 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:34:42 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_RTV1_H
# define RTV1_RTV1_H

# ifdef __APPLE__
#  include <OpenCL/opencl.h>
#  include <OpenCl/cl_platform.h>
# else
#  define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#  include <CL/cl.h>
#  include <CL/cl_platform.h>
# endif

# include "libft/includes/libft.h"
# include <stdlib.h>
# include "vec3.h"
# include "ft_sdl2.h"

# define WINW 1280
# define WINH 720
# define FOV 90
# define EPS 1e-4
# define MAX_DEPTH 3
# define MAX_THREADS 2
# define RADIANS M_PI / 180.0

# define SWAP(a, b) do { typeof(a) t; t = a; a = b; b = t; } while(0)

# define DEL_OBJ ft_memdel((void **)&obj)
# define ADD_OBJ ft_lstadd(&lst, ft_lstnew(obj, sizeof(t_obj3d)))
# define ADD_DEL_OBJ ADD_OBJ; DEL_OBJ

# define SET_KA(a) mat->ka = (a)
# define SET_KD(d) mat->kd = (d)
# define SET_KS(s) mat->ks = (s)
# define SET_SH(c) mat->shininess = (c)
# define SET_ALL_K(a, d, s, c) SET_KA(a); SET_KD(d); SET_KS(s); SET_SH(c)
# define DEL_MAT ft_memdel((void **)&mat)
# define ADD_MAT ft_lstadd(&lst, ft_lstnew(mat, sizeof(t_material)))
# define ADD_DEL_MAT ADD_MAT; DEL_MAT

# define SET_IA(a) light->ia = (a)
# define SET_ID(d) light->id = (d)
# define SET_IS(s) light->is = (s)
# define SET_ALL_I(a, d, s) SET_IA(a); SET_ID(d); SET_IS(s)
# define DEL_LIGHT ft_memdel((void **)&light)
# define ADD_LIGHT ft_lstadd(&lst, ft_lstnew(light, sizeof(t_light)))
# define ADD_DEL_LIGHT ADD_LIGHT; DEL_LIGHT

# define INIT_CAM(i) (init_cam_scene##i)
# define INIT_MAT(i) (init_materials_scene##i)
# define INIT_OBJ(i) (init_objs_scene##i)
# define INIT_LIGHT(i) (init_lights_scene##i)

typedef cl_double3	t_point3;

typedef struct		s_ang
{
	double			deg;
	double			rad;
	double			cos;
	double			sin;
	double			tan;
}					t_ang;

typedef enum		e_type
{
	plane = 0,
	sphere,
	cylinder,
	cone
}					t_type;

typedef struct		s_light
{
	t_vec3			pos;
	int				color;
	double			pow;
	cl_double4		ia;
	cl_double4		id;
	cl_double4		is;
}					t_light;

typedef struct		s_ray
{
	t_vec3			orig;
	t_vec3			dir;
	t_vec3			far;
}					t_ray;

typedef struct		s_cam
{
	t_point3		pos;
	t_vec3			up;
	t_vec3			dir;
	t_ang			roll;
	t_ang			pitch;
	t_ang			yaw;
	t_ang			fov;
	double			ratio;
}					t_cam;

/*
** a - ambient;
** d - diffuse;
** s - specular;
** r - reflection;
** t - transparency;
**
** color - is a temp field, it will be replaced with texture in RT;
**
** sum(asd) + sum(rt) = 1.0
*/
typedef struct		s_material
{
	cl_double4		ka;
	cl_double4		kd;
	cl_double4		ks;
	cl_double4		kr;
	cl_double4		kt;
	double			shininess;
	int				color;
}					t_material;

typedef struct		s_obj3d
{
	void			*data;
	t_type			type;
	t_material		*material;
	t_vec3			normal;
	int				pix_color;
	int				(*intersect)(void *data, t_ray *ray, double *dist,
								t_vec3 *norm);
	int				(*get_color)(struct s_obj3d *obj3d);
}					t_obj3d;

typedef struct		s_scene
{
	t_cam			*cam;
	t_list			*objs;
	t_list			*lights;
	t_list			*materials;
	double			bias;
}					t_scene;

typedef struct		s_env
{
	t_sdl2			*sdl2;
	t_win			*win;
	t_img			*img;
	t_scene			*scene;
}					t_env;

typedef struct		s_sphere
{
	t_point3		pos;
	double			radius;
}					t_sphere;

typedef struct		s_plane
{
	t_vec3			normal;
	t_point3		pos;
}					t_plane;

typedef struct		s_cylinder
{
	t_vec3			pos;
	t_vec3			dir;
	double			radius;
}					t_cylinder;

typedef struct		s_cone
{
	t_vec3			pos;
	t_vec3			dir;
	double			radius;
	double			a;
	double			sin;
	double			cos;
}					t_cone;

typedef struct		s_trace
{
	t_obj3d			*obj;
	t_ray			*ray;
	t_vec3			normal;
	t_point3		interp;
	double			t;
}					t_trace;

typedef struct		s_pthread
{
	t_env			*env;
	int				working_group_offset;
	int				working_group_size;
}					t_pthread;

/*
** boosters.c
*/
int					vec3_color_mult(int a, cl_double4 i);
double				ft_clamp(double v, double s, double e);
int					solve_quadratic(double a, double b, double c, double *dist);

/*
** camera.c
*/
t_cam				*new_cam(t_vec3 pos, cl_double4 ang);
t_ray				cam_getray(t_cam *cam, double x, double y);

/*
** cone.c
*/
void				*cone_delete(t_obj3d **obj3d);
t_obj3d				*cone_init(t_point3 center, t_vec3 dir,
								double a, t_material *mat);

/*
** cylinder.c
*/
void				*cylinder_delete(t_obj3d **obj3d);
t_obj3d				*cylinder_init(t_point3 center, t_vec3 dir, double radius,
									t_material *mat);

/*
** env.c
*/
t_env				*env_init(void);
void				*env_delete(t_env **env);

/*
** light.c
*/
void				light_delete_in_list(void *light, size_t unused);
t_light				*light_init(t_point3 pos, int color);

/*
** material.c
*/
void				*material_delete(t_material **material);
void				material_delete_in_list(void *mat, size_t unused);
t_material			*material_init(int color);

/*
** object.c
*/
void				obj3d_delete_in_list(void *obj3d, size_t unused);
void				*obj3d_delete(t_obj3d **obj3d);

/*
** plane.c
*/
int					plane_get_color(t_obj3d *obj3d);
int					plane_intersect(void *data, t_ray *ray,
									double *t, t_vec3 *normal);
void				*plane_delete(t_obj3d **obj3d);
t_obj3d				*plane_init(t_point3 pos, t_vec3 normal, t_material *mat);

/*
** ray.c
*/
t_point3			ray_comp_point(t_ray *ray, double t);

/*
** render.c
*/
t_obj3d				*comp_intersection(t_ray *ray, t_list *objs, double *t,
										t_vec3 *norm);
int					trace(t_ray *ray, t_scene *scene, int depth);
void				*render_thread(void *t);
void				render(t_env *env);

/*
** scene.c
*/
void				*scene_delete(t_scene **scene);
t_scene				*scene_init(t_list *objs, t_list *lights, t_list *mats,
								t_cam *cam);

/*
** shader.c
*/
t_vec3				diffuse(t_light *light, t_trace *trace, t_ray *ray);
t_vec3				specular(t_light *light, t_trace *trace, t_ray *ray);
t_vec3				phong(t_scene *scene, t_light *light, t_trace *trace);
int					shader(t_scene *scene, t_trace *trace);

/*
** sphere.c
*/
int					sphere_get_color(t_obj3d *obj3d);
int					sphere_intersect(void *data, t_ray *ray,
									double *t, t_vec3 *normal);
void				*sphere_delete(t_obj3d **obj3d);
t_obj3d				*sphere_init(t_point3 center, double radius,
								t_material *mat);

/*
** scenes/init.c
*/
t_scene				*init_scene(int i);

/*
** scenes/sceneX.c
*/
t_scene				*init_scene1(void);
t_scene				*init_scene2(void);
t_scene				*init_scene3(void);

#endif
