NAME = RTv1

RM = /bin/rm
MKDIR = /bin/mkdir
PRINTF = /usr/bin/printf
ECHO = /bin/echo

LIBNAME = ft
LIBDIR = ./libft

INCLUDES = ./include

SOURCES_FOLDER = src/
OBJECTS_FOLDER = obj/

SOURCES = main.c \
			vec3_products.c \
			vec3.c \
			camera.c \
			render.c \
			env.c \
			sphere.c \
			plane.c \
			scene.c \
			object.c \
			light.c \
			boosters.c \
			vec3_other.c \
			ray.c \
			material.c \
			cylinder.c \
			cone.c \
			shader.c \
			scenes/init.c \
			scenes/scene1.c \
			scenes/scene2.c \
			scenes/scene3.c \
			sdl2/sdl2_image.c \
			sdl2/sdl2_init.c \
			sdl2/sdl2_main_loop.c \
			sdl2/sdl2_put_image_to_window.c \
			sdl2/sdl2_window.c

OBJECTS = $(SOURCES:.c=.o)
OBJECTS := $(addprefix $(OBJECTS_FOLDER), $(OBJECTS))
SOURCES := $(addprefix $(SOURCES_FOLDER), $(SOURCES))

CC = clang
AR = ar
CFLAGS = -Wall -Werror -Wextra


# Colors

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m

IFLAGS = -I. -I$(INCLUDES) \
		 -I$(HOME)/.brew/include

LFLAGS = \
		-L$(LIBDIR) -l$(LIBNAME) \
		-L$(HOME)/.brew/lib -lSDL2 -lSDL2_ttf -lSDL2_mixer -lSDL2_image

# Basic Rules

.PHONY: all re clean fclean tests

all: $(NAME)

$(NAME): $(LIBNAME) $(OBJECTS)
	@$(PRINTF) "$(SILENT_COLOR)./$(NAME) binary$(NO_COLOR)"
	@$(CC) $(CFLAGS) $(FFLAGS) $(IFLAGS) $(LFLAGS) \
	-o $(NAME) $(OBJECTS)
	@$(PRINTF)	"\t[$(OK_COLOR)✓$(NO_COLOR)]$(NO_COLOR)\n"

$(OBJECTS_FOLDER)%.o: $(SOURCES_FOLDER)%.c
	@mkdir -p $(OBJECTS_FOLDER)
	@mkdir -p $(OBJECTS_FOLDER)/sdl2 $(OBJECTS_FOLDER)/scenes
	@$(CC) $(CFLAGS) $(IFLAGS) -o $@ -c $<
	@$(PRINTF) "$(OK_COLOR)✓ $(NO_COLOR)$<\n"

$(LIBNAME):
	make -C $(LIBDIR) all

clean:
	@make -C $(LIBDIR) clean
	rm -f $(OBJECTS)
	rm -rf $(OBJECTS_FOLDER)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Objects libftprintf$(NO_COLOR)\n"

fclean: clean
	make -C $(LIBDIR) fclean
	rm -f $(NAME)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Library libftprintf$(NO_COLOR)\n"

re: fclean all