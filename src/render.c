/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:35 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:35 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include <OpenCL/opencl.h>
#include "include/rtv1.h"

#define C_OBJ3D ((t_obj3d *)curr->content)
#define T ((t_pthread *) t)

t_obj3d	*comp_intersection(t_ray *ray, t_list *objs, double *t, t_vec3 *norm)
{
	t_obj3d	*obj;
	t_list	*curr;
	t_vec3	normal;
	double	n;

	obj = NULL;
	curr = objs;
	while (curr)
	{
		if (C_OBJ3D->intersect(C_OBJ3D->data, ray, &n, &normal) && n < *t)
		{
			*t = n;
			obj = C_OBJ3D;
			(*norm) = vec3_normalized(normal);
		}
		curr = curr->next;
	}
	return (obj);
}

int		trace(t_ray *ray, t_scene *scene, int depth)
{
	t_trace	trace;

	(void)depth;
	trace.t = INFINITY;
	trace.obj = comp_intersection(ray, scene->objs, &trace.t, &trace.normal);
	if (trace.obj)
	{
		trace.ray = ray;
		trace.interp = ray_comp_point(ray, trace.t);
		trace.obj->pix_color = shader(scene, &trace);
		return (trace.obj->pix_color);
	}
	return (0);
}

void	*render_thread(void *t)
{
	cl_int2		c;
	cl_double2	n;
	cl_double2	p;
	t_ray		ray;
	int			color;

	p.s0 = (double)WINW / (double)WINH;
	p.s1 = tan(T->env->scene->cam->fov.rad / 2.0);
	c.y = T->working_group_offset - 1;
	while (++c.y < T->working_group_offset + T->working_group_size)
	{
		n.y = (1 - 2 * ((c.y + 0.5) / (double)WINH)) * p.s1;
		c.x = -1;
		while (++c.x < WINW)
		{
			n.x = (2 * ((c.x + 0.5) / (double)WINW) - 1) * p.s1 * p.s0;
			ray = cam_getray(T->env->scene->cam, n.x, n.y);
			color = trace(&ray, T->env->scene, MAX_DEPTH);
			sdl2_img_put_pixel(T->env->img, c.x, c.y, (Uint32)color);
		}
	}
	return (NULL);
}

void	render(t_env *env)
{
	t_pthread	t[MAX_THREADS + 1];
	pthread_t	tid[MAX_THREADS + 1];
	int			i;

	i = -1;
	while (++i < MAX_THREADS + 1)
	{
		t[i].env = env;
		t[i].working_group_size = WINH / (MAX_THREADS + 1);
		t[i].working_group_offset = i * t[i].working_group_size;
	}
	i = -1;
	while (++i < MAX_THREADS)
		pthread_create(&tid[i], NULL, render_thread, &t[i]);
	render_thread(&t[MAX_THREADS]);
	i = -1;
	while (++i < MAX_THREADS)
		pthread_join(tid[i], NULL);
}
