/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   boosters.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:03 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:04 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "include/rtv1.h"

int		vec3_color_mult(int a, cl_double4 i)
{
	t_color	c;

	c.color = a;
	c.argb.r = ft_ilerp(0, c.argb.r, i.s0);
	c.argb.g = ft_ilerp(0, c.argb.g, i.s0);
	c.argb.b = ft_ilerp(0, c.argb.b, i.s0);
	c.argb.a = 0xff;
	return (c.color);
}

double	ft_clamp(double v, double s, double e)
{
	if (v < s)
		return (s);
	else if (v > e)
		return (e);
	return (v);
}

/*
** solves quadratic equation and sets minimal positive (if exist)
** solution in dist param
** returns:
** 			0 - if solution not exist;
** 			1 - if solution exist;
*/

int		solve_quadratic(double a, double b, double c, double *dist)
{
	double		d;
	cl_double2	x;

	d = b * b - 4.0 * a * c;
	if (d < 0.0)
		return (0);
	d = sqrt(d);
	x.s0 = (b - d) / (2.0 * a);
	x.s1 = (b + d) / (2.0 * a);
	if (x.s0 > x.s1 && x.s1 > 0.0)
		SWAP(x.s0, x.s1);
	*dist = x.s0;
	return (1);
}
