/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 05:55:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/22 05:55:02 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

t_list	*init_objs_scene1(t_list *mats)
{
	t_list	*lst;
	t_obj3d	*obj;

	lst = NULL;
	obj = sphere_init(vec3_new(-50, 10, 30), 20, mats->next->content);
	ADD_DEL_OBJ;
	obj = sphere_init(vec3_new(50, 0, 80), 30, mats->next->next->content);
	ADD_DEL_OBJ;
	obj = plane_init(vec3_new(0, -31, 0), vec3_new(0, 1, 0), mats->content);
	ADD_DEL_OBJ;
	obj = plane_init(vec3_new(0, 0, 111), vec3_new(0, 0, -1), mats->content);
	ADD_DEL_OBJ;
	obj = cylinder_init(vec3_new(0, 0, 80), vec3_new(1, 1, 0),
						20, mats->next->next->next->content);
	ADD_DEL_OBJ;
	obj = cone_init(vec3_new(0, 0, 50), vec3_new(-1, 1.5, 1),
					20, mats->content);
	ADD_DEL_OBJ;
	return (lst);
}

t_list	*init_materials_scene1(void)
{
	t_list		*lst;
	t_material	*mat;

	lst = NULL;
	mat = material_init(0);
	SET_ALL_K(vec3_new(0.0, 0.0, 0.0), vec3_new(0.1, 0.1, 0.1),
				vec3_new(0.5, 0.5, 0.5), 32);
	ADD_DEL_MAT;
	mat = material_init(0xFFE3DEDB);
	SET_ALL_K(vec3_new(0.25, 0.25, 0.25), vec3_new(0.4, 0.4, 0.4),
				vec3_new(0.774579, 0.774579, 0.774579), 76.8);
	ADD_DEL_MAT;
	mat = material_init(0xFFC0C0C0);
	SET_ALL_K(vec3_new(0.19225, 0.19225, 0.19225),
				vec3_new(0.50754, 0.50754, 0.50754),
				vec3_new(0.508273, 0.508273, 0.508273), 51.2);
	ADD_DEL_MAT;
	mat = material_init(0xFFC0C0C0);
	SET_ALL_K(vec3_new(0.2295, 0.08825, 0.0275),
				vec3_new(0.5508, 0.2118, 0.066),
				vec3_new(0.580594, 0.223257, 0.0695701), 51.2);
	ADD_DEL_MAT;
	return (lst);
}

t_list	*init_lights_scene1(void)
{
	t_list		*lst;
	t_light		*light;

	lst = NULL;
	light = light_init(vec3_new(-200, 50, 0), 0xffff0000);
	SET_ALL_I(vec3_new(0.5, 0.5, 0.5),
				vec3_new(0.8, 0.8, 0.8), vec3_new(0.4, 0.4, 0.4));
	ADD_DEL_LIGHT;
	light = light_init(vec3_new(200, 50, 0), 0xffffffff);
	SET_ALL_I(vec3_new(0.5, 0.5, 0.5),
				vec3_new(0.8, 0.8, 0.8), vec3_new(0.4, 0.4, 0.4));
	ADD_DEL_LIGHT;
	return (lst);
}

t_cam	*init_cam_scene1(void)
{
	t_cam		*cam;
	cl_double4	ang;

	ang.s0 = 90;
	ang.s1 = 0;
	ang.s2 = -45;
	ang.s3 = 45;
	if (!(cam = new_cam(vec3_new(100, 100, -100), ang)))
		return (NULL);
	return (cam);
}

t_scene	*init_scene1(void)
{
	t_scene	*scene;

	if (!(scene = scene_init(NULL, NULL, NULL, NULL)))
		return (NULL);
	scene->bias = 1e-4;
	if (!(scene->cam = INIT_CAM(1)()))
		return (scene_delete(&scene));
	if (!(scene->materials = INIT_MAT(1)()))
		return (scene_delete(&scene));
	if (!(scene->objs = INIT_OBJ(1)(scene->materials)))
		return (scene_delete(&scene));
	if (!(scene->lights = INIT_LIGHT(1)()))
		return (scene_delete(&scene));
	return (scene);
}
