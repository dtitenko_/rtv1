/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:43 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:43 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>
#include <libft/includes/libft.h>

#define SPHERE ((t_sphere *)data)

int			sphere_get_color(t_obj3d *obj3d)
{
	if (!(obj3d && obj3d->material))
		return (0);
	return (obj3d->material->color | AMASK);
}

int			sphere_intersect(void *data, t_ray *ray, double *t, t_vec3 *normal)
{
	t_vec3		pos;
	cl_double3	p;

	pos = vec3_sub(SPHERE->pos, ray->orig);
	p.s0 = vec3_dot(ray->dir, ray->dir);
	p.s1 = 2.0 * vec3_dot(ray->dir, pos);
	p.s2 = vec3_dot(pos, pos)
			- SPHERE->radius * SPHERE->radius;
	if (!solve_quadratic(p.s0, p.s1, p.s2, t))
		return (0);
	if (*t < EPS)
		return (0);
	p = ray_comp_point(ray, *t);
	(*normal) = vec3_normalized(vec3_sub(p, SPHERE->pos));
	return (1);
}

void		*sphere_delete(t_obj3d **obj3d)
{
	ft_memdel(&(*obj3d)->data);
	ft_memdel((void **)obj3d);
	return (NULL);
}

t_obj3d		*sphere_init(t_point3 center, double radius, t_material *mat)
{
	t_obj3d		*obj3d;
	t_sphere	*p_sphere;

	if (!(obj3d = ft_memalloc(sizeof(t_obj3d))))
		return (NULL);
	if (!(p_sphere = ft_memalloc(sizeof(t_sphere))))
		return (obj3d_delete(&obj3d));
	p_sphere->pos = center;
	p_sphere->radius = radius;
	obj3d->data = p_sphere;
	obj3d->type = sphere;
	obj3d->material = mat;
	obj3d->intersect = &sphere_intersect;
	obj3d->get_color = &sphere_get_color;
	return (obj3d);
}
