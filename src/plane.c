/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plane.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:28 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:28 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

#define PLANE ((t_plane *)data)

int			plane_get_color(t_obj3d *obj3d)
{
	if (!(obj3d && obj3d->material))
		return (0);
	return (obj3d->material->color);
}

int			plane_intersect(void *data, t_ray *ray, double *t, t_vec3 *normal)
{
	t_vec3	w;
	double	d;

	d = vec3_dot(PLANE->normal, ray->dir);
	if (fabs(d) < EPS)
		return (0);
	w = vec3_sub(ray->orig, PLANE->pos);
	d = -vec3_dot(PLANE->normal, w) / d;
	if (d < 0.0)
		return (0);
	*t = d;
	(*normal) = PLANE->normal;
	w = ray_comp_point(ray, d);
	if (vec3_dot(vec3_sub(w, ray->orig), ray->dir) < 0)
		return (0);
	return (1);
}

void		*plane_delete(t_obj3d **obj3d)
{
	ft_memdel(&(*obj3d)->data);
	ft_memdel((void **)obj3d);
	return (NULL);
}

t_obj3d		*plane_init(t_point3 pos, t_vec3 normal, t_material *mat)
{
	t_obj3d		*obj3d;
	t_plane		*p_plane;

	if (!(obj3d = ft_memalloc(sizeof(t_obj3d))))
		return (NULL);
	if (!(p_plane = ft_memalloc(sizeof(t_plane))))
		return (obj3d_delete(&obj3d));
	p_plane->normal = vec3_normalized(normal);
	p_plane->pos = pos;
	obj3d->data = p_plane;
	obj3d->type = plane;
	obj3d->material = mat;
	obj3d->get_color = &plane_get_color;
	obj3d->intersect = &plane_intersect;
	return (obj3d);
}
