/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_products.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:51 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:51 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/rtv1.h"
#include "libft/includes/libft.h"

double	vec3_dot(t_vec3 v1, t_vec3 v2)
{
	double		ret;
	int			i;

	ret = 0.0;
	i = -1;
	while (++i < 3)
		ret += v1.s[i] * v2.s[i];
	return (ret);
}

t_vec3	vec3_scale(t_vec3 v, double s)
{
	t_vec3	ret;
	int		i;

	i = -1;
	while (++i < 3)
		ret.s[i] = v.s[i] * s;
	return (ret);
}

t_vec3	vec3_cross(t_vec3 v1, t_vec3 v2)
{
	t_vec3	ret;

	ret.x = v1.y * v2.z + v2.y * v1.z;
	ret.y = v1.x * v2.z + v2.x * v1.z;
	ret.z = v1.x * v2.y + v2.x * v1.y;
	return (ret);
}

t_vec3	vec3_product(t_vec3 v1, t_vec3 v2)
{
	t_vec3	tmp;
	int		i;

	i = -1;
	while (++i < 3)
		tmp.s[i] = v1.s[i] * v2.s[i];
	return (tmp);
}

double	vec3_cos(t_vec3 v1, t_vec3 v2)
{
	return (vec3_dot(v1, v2) /
			sqrt(sqrt(vec3_dot(v1, v1)) * sqrt(vec3_dot(v2, v2))));
}
