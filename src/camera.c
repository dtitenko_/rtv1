/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:06 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:06 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>

#define IANG_RAD(ang) ((ang).rad = (ang).deg * RADIANS)
#define IANG_COS(ang) ((ang).cos = cos((ang).rad))
#define IANG_SIN(ang) ((ang).sin = sin((ang).rad))
#define IANG_TAN(ang) ((ang).tan = tan((ang).rad))
#define INIT_ANG(a) IANG_RAD(a); IANG_COS(a); IANG_SIN(a); IANG_TAN(a)

#define PITCH (cam->pitch)
#define ROLL (cam->roll)
#define YAW (cam->yaw)

/*
** creates new camera with position in pos and angles
** angles:
** 		1) fov;
** 		2) roll;
** 		3) pitch;
** 		4) yaw;
*/

t_cam	*new_cam(t_vec3 pos, cl_double4 ang)
{
	t_cam	*cam;

	if (!(cam = ft_memalloc(sizeof(t_cam))))
		return (NULL);
	cam->pos = pos;
	cam->fov.deg = ang.s0;
	cam->roll.deg = ang.s1;
	cam->pitch.deg = ang.s2;
	cam->yaw.deg = ang.s3;
	INIT_ANG(cam->fov);
	INIT_ANG(cam->roll);
	INIT_ANG(cam->pitch);
	INIT_ANG(cam->yaw);
	cam->ratio = (double)WINW / (double)WINH;
	cam->dir = cam_getray(cam, 0, 0).dir;
	return (cam);
}

/*
** constructs new ray from camera position to x and y in NDC
*/

t_ray	cam_getray(t_cam *cam, double x, double y)
{
	t_ray	ray;

	ray.dir.x = -PITCH.cos * YAW.sin + x * YAW.cos + y * PITCH.sin * YAW.sin;
	ray.dir.y = PITCH.sin + y * PITCH.cos;
	ray.dir.z = PITCH.cos * YAW.cos + x * YAW.sin - y * PITCH.sin * YAW.cos;
	ray.dir = vec3_normalized(ray.dir);
	ray.orig = cam->pos;
	return (ray);
}
