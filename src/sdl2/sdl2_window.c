/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl2_window.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:33:16 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:33:17 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>
#include "libft/includes/libft.h"
#include "include/ft_sdl2.h"

static void	*on_error_log(char *fmt, t_win **win)
{
	SDL_Log(fmt, SDL_GetError());
	sdl2_delete_window(win);
	return (NULL);
}

t_win		*sdl2_new_win(t_sdl2 *sdl2, const char *title, int w, int h)
{
	t_win		*new_win;

	if (!(new_win = ft_memalloc(sizeof(t_win))))
		return (NULL);
	new_win->window = NULL;
	new_win->renderer = NULL;
	if (!(new_win->window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_SHOWN)))
	{
		return (on_error_log("Unable to create window: %s",
							&new_win));
	}
	if (!(new_win->renderer = SDL_CreateRenderer(new_win->window, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)))
	{
		return (on_error_log("Unable to create renderer: %s", &new_win));
	}
	SDL_SetWindowTitle(new_win->window, title);
	new_win->next = sdl2->win;
	sdl2->win = new_win;
	return (new_win);
}

void		*sdl2_delete_window(t_win **win)
{
	if (!win || !(*win))
		return (NULL);
	SDL_DestroyRenderer((*win)->renderer);
	SDL_DestroyWindow((*win)->window);
	ft_memdel((void **)win);
	return (NULL);
}

void		*sdl2_destroy_window(t_sdl2 *sdl2, t_win *win)
{
	t_win	*prev;
	t_win	*curr;

	if (!(sdl2 && win))
		return (NULL);
	curr = sdl2->win;
	prev = NULL;
	while (curr)
	{
		if (curr == win)
		{
			if (prev)
				prev->next = curr->next;
			else
				sdl2->win = curr->next;
			break ;
		}
		prev = curr;
		curr = curr->next;
	}
	if (curr)
		sdl2_delete_window(&curr);
	return (NULL);
}
