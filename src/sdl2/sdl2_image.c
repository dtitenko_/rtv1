/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl2_image.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:33:00 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:33:02 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "include/ft_sdl2.h"

static void	*on_error_log(char *fmt, t_img **img)
{
	SDL_Log(fmt, SDL_GetError());
	return (sdl2_destroy_img(img));
}

void		*sdl2_destroy_img(t_img **img)
{
	if (!(img && *img))
		return (NULL);
	if ((*img)->surface)
		SDL_FreeSurface((*img)->surface);
	if ((*img)->texture)
		SDL_DestroyTexture((*img)->texture);
	if ((*img)->pixels)
		ft_memdel((void **)&((*img)->pixels));
	ft_memdel((void **)img);
	return (*img);
}

t_img		*sdl2_new_img(t_win *win, int w, int h)
{
	t_img	*img;

	if (!(w > 0 && h > 0))
		return (NULL);
	if (!(img = ft_memalloc(sizeof(t_img))))
		return (NULL);
	if (!(img->pixels = ft_memalloc((sizeof(Uint32) * w * h))))
		return (sdl2_destroy_img(&img));
	if (!(img->surface = SDL_CreateRGBSurface(
		0, w, h, 32, RMASK, GMASK, BMASK, AMASK)))
		return (on_error_log("SDL_CreateRGBSurface error: %s", &img));
	if (!(img->texture = SDL_CreateTextureFromSurface(win->renderer,
													img->surface)))
		return (on_error_log("SDL_CreateTextureFromSurface error: %s", &img));
	img->bpp = 32;
	img->width = w;
	img->height = h;
	return (img);
}

void		sdl2_img_put_pixel(t_img *img, int x, int y, Uint32 color)
{
	if (0 > x || x >= img->width || 0 > y || y >= img->height)
		return ;
	img->pixels[x + y * img->width] = color | AMASK;
}

Uint32		sdl2_img_get_pixel(t_img *img, int x, int y)
{
	if (0 > x || x >= img->width || 0 > y || y >= img->height)
		return (0);
	return (img->pixels[x + y * img->width]);
}
