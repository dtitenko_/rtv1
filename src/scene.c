/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:37 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:38 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/rtv1.h>
#include <libft/includes/libft.h>

void		*scene_delete(t_scene **scene)
{
	ft_lstdel(&(*scene)->objs, &obj3d_delete_in_list);
	ft_lstdel(&(*scene)->lights, &light_delete_in_list);
	ft_lstdel(&(*scene)->materials, &material_delete_in_list);
	ft_memdel((void **)&(*scene)->cam);
	ft_memdel((void **)scene);
	return (NULL);
}

t_scene		*scene_init(t_list *objs, t_list *lights, t_list *mats, t_cam *cam)
{
	t_scene	*scene;

	if (!(scene = ft_memalloc(sizeof(t_scene))))
		return (NULL);
	scene->lights = lights;
	scene->objs = objs;
	scene->materials = mats;
	scene->cam = cam;
	return (scene);
}
