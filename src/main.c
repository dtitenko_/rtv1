/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 17:33:42 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/12 17:33:43 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "include/rtv1.h"
#include "include/ft_sdl2.h"

#define ENV ((t_env *)env)

int			sdl_event_filter(void *env, SDL_Event *e)
{
	int		d;

	(void)env;
	if (e->type == SDL_MOUSEBUTTONDOWN)
		(void)e;
	else if (e->type == SDL_MOUSEWHEEL && !(!e->wheel.x && !e->wheel.y))
	{
		d = e->wheel.y;
		ENV->scene->cam->pos = vec3_sum(ENV->scene->cam->pos,
				vec3_product(ENV->scene->cam->dir,
							vec3_new(d * 3, d * 3, d * 3)));
		render(ENV);
		sdl2_put_image_to_window(ENV->win, ENV->img, 0, 0);
	}
	return (1);
}

int			main1(int argc, char **argv)
{
	t_env			*env;

	(void)argc;
	(void)argv;
	SDL_Init(SDL_INIT_EVERYTHING);
	if (!(env = env_init()))
		return (EXIT_FAILURE);
	if (!(env->scene = init_scene(1)))
		return (env_delete(&env) == NULL);
	render(env);
	SDL_SetEventFilter(sdl_event_filter, env);
	sdl2_put_image_to_window(env->win, env->img, 0, 0);
	sdl2_main_loop(env->sdl2);
	scene_delete(&env->scene);
	env_delete(&env);
	return (EXIT_SUCCESS);
}

int			main(int argc, char **argv)
{
	main1(argc, argv);
	return (0);
}
