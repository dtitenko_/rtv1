/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_other.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:48 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:48 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/vec3.h>

t_vec3	vec3_sum(t_vec3 v1, t_vec3 v2)
{
	t_vec3	tmp;
	int		i;

	i = -1;
	while (++i < 3)
		tmp.s[i] = v1.s[i] + v2.s[i];
	return (tmp);
}

t_vec3	vec3_sub(t_vec3 v1, t_vec3 v2)
{
	t_vec3	tmp;
	int		i;

	i = -1;
	while (++i < 3)
		tmp.s[i] = v1.s[i] - v2.s[i];
	return (tmp);
}

t_vec3	vec3_div(t_vec3 v1, t_vec3 v2)
{
	t_vec3	tmp;
	int		i;

	i = -1;
	while (++i < 3)
		tmp.s[i] = v1.s[i] / v2.s[i];
	return (tmp);
}

t_vec3	vec3_add_num(t_vec3 v1, double v)
{
	t_vec3	tmp;
	int		i;

	i = -1;
	while (++i < 3)
		tmp.s[i] = v1.s[i] + v;
	return (tmp);
}

t_vec3	vec3_sub_num(t_vec3 v1, double v)
{
	t_vec3	tmp;
	int		i;

	i = -1;
	while (++i < 3)
		tmp.s[i] = v1.s[i] - v;
	return (tmp);
}
