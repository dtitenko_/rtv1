/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   material.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 04:14:22 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/19 04:14:22 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <OpenCL/opencl.h>
#include "include/rtv1.h"

void		*material_delete(t_material **material)
{
	ft_memdel((void **)material);
	return (NULL);
}

/*
** wrapper for material_delete for ft_lstdel function
*/

void		material_delete_in_list(void *mat, size_t unused)
{
	(void)unused;
	material_delete((t_material **)&mat);
}

/*
** inits material with color and all coefficients set to 0
*/

t_material	*material_init(int color)
{
	t_material	*material;

	if (!(material = ft_memalloc(sizeof(t_material))))
		return (NULL);
	material->color = color;
	ft_bzero(material->ka.s, sizeof(cl_double) * 4);
	ft_bzero(material->kd.s, sizeof(cl_double) * 4);
	ft_bzero(material->ks.s, sizeof(cl_double) * 4);
	ft_bzero(material->kr.s, sizeof(cl_double) * 4);
	ft_bzero(material->kt.s, sizeof(cl_double) * 4);
	material->shininess = 0.0;
	return (material);
}
