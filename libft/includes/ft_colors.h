/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_colors.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:01:04 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 18:28:25 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_COLORS_H
# define FT_COLORS_H

# define COLOR_BLEND_OVER		0
# define COLOR_BLEND_ADD		1
# define COLOR_BLEND_SUB		2
# define COLOR_BLEND_MUL		3
# define COLOR_BLEND_DIV		4
# define COLOR_BLEND_HARD_LIGHT	5
# define COLOR_BLEND_SOFT_LIGHT	6

# define BLACK		0
# define RED		1
# define GREEN		2
# define YELLOW		3
# define BLUE		4
# define MAGENTA	5
# define CYAN		6
# define WHITE		7

# include <stdlib.h>

typedef union			u_color
{
	struct				s_rgb
	{
		unsigned char	b;
		unsigned char	g;
		unsigned char	r;
		unsigned char	a;
	}					argb;
	int					color;
}						t_color;

int						ft_rgb2int(int r, int g, int b);
t_color					fade_color(t_color a, float opacity);
int						blend_colors(int color1, int color2,
										float coeff, int type);
t_color					color_blend_over(t_color a, t_color b);
t_color					color_blend_add(t_color a, t_color b);
int						icolor_blend_add(int a, int b);
t_color					color_blend_sub(t_color a, t_color b);

/*
** ft_clerp.c
*/
t_color					ft_clerp(t_color c1, t_color c2, double t);
int						ft_iclerp(int c1, int c2, double t);

t_color					color_mult(t_color a, t_color b);
int						icolor_mult(int a, int b);

#endif
