cmake_minimum_required(VERSION 3.5)

project(libft C)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/includes)

set(CMAKE_C_FLAGS "-Wall -Wextra -Werror")

set(SRC_MEM
		./src/memory/ft_memset.c
		./src/memory/ft_memcpy.c
		./src/memory/ft_memccpy.c
		./src/memory/ft_memchr.c
		./src/memory/ft_memcmp.c
		./src/memory/ft_memdel.c
		./src/memory/ft_memmove.c
		./src/memory/ft_bzero.c
		./src/memory/ft_memdup.c
		./src/memory/ft_memalloc.c)

set(SRC_CLRS
		./src/colors/blend_colors.c
		./src/colors/color_blend_over.c
		./src/colors/color_blend_sub.c
		./src/colors/fade_color.c
		./src/colors/color_blend_add.c
		./src/colors/ft_clerp.c
		./src/colors/ft_rgb2int.c)

set(SRC_INPT
		./src/input/ft_getchar.c
		./src/input/get_next_line.c)

set(SRC_STR
		./src/strings/ft_isdigit.c
		./src/strings/ft_strlcat.c
		./src/strings/ft_strncpy.c
		./src/strings/ft_strnstr.c
		./src/strings/ft_strdel.c
		./src/strings/ft_strsplit.c
		./src/strings/ft_isprint.c
		./src/strings/ft_strchr.c
		./src/strings/ft_isalpha.c
		./src/strings/ft_strstr.c
		./src/strings/ft_strcat.c
		./src/strings/ft_strlen.c
		./src/strings/ft_strcmp.c
		./src/strings/ft_strcpy.c
		./src/strings/ft_strtrim.c
		./src/strings/ft_strnew.c
		./src/strings/ft_strclr.c
		./src/strings/ft_strrchr.c
		./src/strings/ft_strncmp.c
		./src/strings/ft_strmap.c
		./src/strings/ft_strnequ.c
		./src/strings/ft_strdup.c
		./src/strings/ft_isalnum.c
		./src/strings/ft_striter.c
		./src/strings/ft_strjoin.c
		./src/strings/ft_strequ.c
		./src/strings/ft_strmapi.c
		./src/strings/ft_isascii.c
		./src/strings/ft_strsub.c
		./src/strings/ft_striteri.c
		./src/strings/ft_strncat.c
		./src/strings/ft_instr.c
		./src/strings/ft_count_whitespaces.c
		./src/strings/ft_strsplitdel.c)

set(SRC_LST
		./src/lists/ft_lstiter.c
		./src/lists/ft_lstqueueadd.c
		./src/lists/ft_lstrev.c
		./src/lists/ft_lstnew.c
		./src/lists/ft_lstmap.c
		./src/lists/ft_lstadd.c
		./src/lists/ft_lstfreeto.c
		./src/lists/ft_lstdel.c
		./src/lists/ft_lstdelone.c
		./src/lists/ft_lstlen.c)

set(SRC_PRNT
		./src/print/ft_putendl.c
		./src/print/ft_putendl_fd.c
		./src/print/exit_with_error.c
		./src/print/ft_putchar_fd.c
		./src/print/ft_putchar.c
		./src/print/ft_putstr.c
		./src/print/ft_putnbr.c
		./src/print/ft_putlnbr.c
		./src/print/ft_putnbr_fd.c
		./src/print/ft_putnbrb.c
		./src/print/ft_putstr_fd.c)

set(SRC_CNVRT
		./src/convert/ft_itoa.c
		./src/convert/ft_ltoa_base.c
		./src/convert/ft_atoi.c
		./src/convert/ft_tolower.c
		./src/convert/ft_toupper.c
		./src/convert/ft_wctomb.c
		./src/convert/ft_wcstombs.c
		./src/convert/ft_atol_base.c
		./src/convert/ft_atof.c
		./src/convert/ft_ftoa.c
		./src/convert/ft_ftoa_s.c
		./src/convert/ft_ulltoa_base.c)

set(SRC_MATH
		./src/math/ft_abs.c
		./src/math/ft_lerp.c
		./src/math/ft_pow.c
		./src/math/ft_powf.c
		./src/math/ft_round.c)

set(SRC	${SRC_MEM} ${SRC_CLRS} ${SRC_INPT} ${SRC_STR}
		${SRC_LST} ${SRC_PRNT} ${SRC_CNVRT} ${SRC_MATH} src/math/ft_round.c src/colors/color_mult.c)

set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR})
set(CMAKE_CURRENT_BINARY_DIR ${CMAKE_CURRENT_SOURCE_DIR})

add_library(ft ${SRC})

#add_executable(test_t_color		tests/colors/test_t_color.c)
#add_executable(test_converters	tests/convert/test_long_converters.c)
#add_executable(test_ft_atof		tests/convert/test_ft_atof.c)
#add_executable(test_ft_pow		tests/math/test_ft_pow.c)
#add_executable(test_ft_powf		tests/math/test_ft_powf.c)
#add_executable(test_ft_ftoa		tests/convert/test_ft_ftoa.c)
#add_executable(test_ft_ftoa_s	tests/convert/test_ft_ftoa_s.c)
#add_executable(test_ft_wctomb	tests/convert/test_ft_wctomb.c)

#target_link_libraries(test_converters	ft)
#target_link_libraries(test_ft_atof		ft)
#target_link_libraries(test_t_color		ft)
#target_link_libraries(test_ft_pow		ft m)
#target_link_libraries(test_ft_powf		ft m)
#target_link_libraries(test_ft_ftoa		ft)
#target_link_libraries(test_ft_ftoa_s	ft)
#target_link_libraries(test_ft_wctomb	ft)
