/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 12:10:30 by dtitenko          #+#    #+#             */
/*   Updated: 2017/02/01 12:12:23 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_parse_exp(char *str)
{
	int		exp;
	double	n;

	if (!str)
		return (0);
	if (ft_tolower(*str) != 'e')
		return (1);
	str += (ft_tolower(*str) == 'e') ? 1 : 0;
	exp = ft_atoi(str);
	n = (double)ft_powf(10, exp);
	return (n);
}

double	ft_atof(char *str)
{
	double intp;
	double frac;
	double decp;
	double sign;

	if (!str)
		return (0);
	str += ft_count_whitespaces(str);
	sign = (*str == '-') ? -1 : 1;
	str += (*str == '-' || *str == '+') ? 1 : 0;
	intp = 0;
	while (ft_isdigit(*str))
		intp = intp * 10 + (*str++ - '0');
	str += (*str == '.') ? 1 : 0;
	frac = 1;
	decp = 0;
	while (ft_isdigit(*str))
	{
		decp = decp * 10 + (*str++ - '0');
		frac *= 10;
	}
	decp /= frac;
	frac = ft_parse_exp(str);
	return (sign * ((intp + decp) * frac));
}
