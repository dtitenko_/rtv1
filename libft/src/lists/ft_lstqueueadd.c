/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstqueueadd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:45:08 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:48:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_lists.h>

void	ft_lstqueueadd(t_list **alst, t_list *new)
{
	t_list	*current;

	if (new && alst)
	{
		current = *alst;
		if (current)
		{
			while (current->next)
				current = current->next;
			current->next = new;
		}
		else
			ft_lstadd(alst, new);
	}
}
