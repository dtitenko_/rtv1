/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lerp.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:46:13 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:46:14 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

double	ft_dlerp(double v0, double v1, double t)
{
	return (v0 + t * (v1 - v0));
}

float	ft_flerp(float v0, float v1, float t)
{
	return ((float)ft_dlerp(v0, v1, t));
}

int		ft_ilerp(int v0, int v1, double t)
{
	return ((int)ft_dlerp(v0, v1, t));
}
